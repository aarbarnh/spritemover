﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class playerController : MonoBehaviour //player controller class
{
    private Rigidbody2D rb2d; //private rigibody 2d object to interact with sprite's rigidbody 
    bool faceRight = true; //variable to test if right or left, flip the sprite
    bool movementOn = true; //variable to enable moving sprite 
    public float moveSpeed = 0.25f; //max speed the sprite can move

	// Use this for initialization
	void Start () //run on start, when object is created 
    {
        rb2d = GetComponent<Rigidbody2D>(); //get the rigibody component, hold in rb2d object
	}
	
	// Update on a fixed rate 
	void FixedUpdate ()
    {
        float moveHorizontal = Input.GetAxisRaw("Horizontal"); //horizontal movement got by horizontal axis input 
        float moveVertical = Input.GetAxisRaw("Vertical"); //vertical movement got by vertical axis input 
        Vector2 movement = new Vector2(moveHorizontal, moveVertical); //2d vector for movement with both vertical and horizontal movement as parameters 

        if (Input.GetKeyDown(KeyCode.P) && movementOn == true) //if P is pressed while movement is on 
            movementOn = false; //movement is false 
        else if (Input.GetKeyDown(KeyCode.P) && movementOn == false) //if P is pressed while movement is off
            movementOn = true; //movement is true  

        if (Input.GetKeyDown(KeyCode.Q)) //if Q is pressed while sprite is active 
        {
            gameObject.SetActive(false); //sets the gameobject (sprite) inactive
        }


        //reseting sprite to origin 0,0 when space is pressed
        if (Input.GetKeyDown(KeyCode.Space))
        {
            transform.position = new Vector2(0.0f, 0.0f);
        }


        //conditional on whether movement is on or not
        if (movementOn == true && Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift)) //if left shift is held down while you can move 
        {
            if (Input.GetKeyDown(KeyCode.A) || Input.GetKeyDown(KeyCode.LeftArrow)) //move left one unit at a time 
            {
                gameObject.transform.position += Vector3.left;
                if (faceRight) //flip the sprite when moving left and facing right
                {
                    flipSprite();
                }
            }
            else if (Input.GetKeyDown(KeyCode.S) || Input.GetKeyDown(KeyCode.DownArrow)) //move down one unit at a time
            {
                gameObject.transform.position += Vector3.down;
            }
            else if (Input.GetKeyDown(KeyCode.D) || Input.GetKeyDown(KeyCode.RightArrow)) //move right one unit at a time 
            {
                gameObject.transform.position += Vector3.right;
                if (!faceRight) //flip sprite if facing left while trying to go right
                {
                    flipSprite();
                }
            }
            else if (Input.GetKeyDown(KeyCode.W) || Input.GetKeyDown(KeyCode.UpArrow)) //move up one unit at a time
            {
                gameObject.transform.position += Vector3.up;
            }
        }
        else if (movementOn == true) //if movement is true but no shift held sprite can move fluidly
        {
            rb2d.MovePosition(rb2d.position + movement * moveSpeed); //moving the sprite across the screen, taking its current position + the movement * its speed
            if (moveHorizontal > 0 && !faceRight) //if facing left and moving to the right 
                flipSprite(); //flip sprite 
            else if (moveHorizontal < 0 && faceRight) //if facing right and moving left 
                flipSprite(); //flip sprite
        } 

    }

    void flipSprite() //function to flip sprite image 
    {
        faceRight = !faceRight; //change faceRight bool to the opposite of what it is now
        Vector3 theScale = transform.localScale; //get the sprites transform
        theScale.x *= -1; //flip the transform which flips the sprite
        transform.localScale = theScale; //give the sprites transform the new inversed value
    }
}
