# README #

Name: Aaron Barnhart
Project: Sprite Mover
Class: Game Programming Intro

This program is a simple game where you can move a sprite back and forth.
P disables the movement.
Q makes the sprite game object inactive.
ESC quits the build (application, .exe).
Space returns the sprite to the center of the screen (origin).
Holding Shift while using arrows or wasd will move one unit at a time.

The doom guy sprite and the mars-esque ground were both made by me on pixilart.com